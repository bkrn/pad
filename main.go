package main

import (
	"flag"

	"gitlab.com/note32/server"
	"gitlab.com/note32/settings"
)

func main() {
	cfgpth := flag.String("cfg", "config-prod.toml", "set the config file for use")
	flag.Parse()

	settings.SetConfig(*cfgpth)
	srv := server.GetServer(nil)
	srv.Start(":5000")
}
