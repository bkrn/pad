package settings

import (
	"github.com/BurntSushi/toml"
)

//AppCfg holds global application settings
var AppCfg Config

//Store holds settings for store objects
type Store struct {
	Region string
	Bucket string
}

//Config holds application settings
type Config struct {
	Store Store
	Env   string
}

//SetConfig inits the config
func SetConfig(pth string) error {
	AppCfg = Config{}
	_, err := toml.DecodeFile(pth, &AppCfg)
	return err
}
