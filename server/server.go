package server

import (
	"net/http"

	"gitlab.com/note32/control"
	"gitlab.com/note32/store"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

//Server a wrapper for the underlying server
//right now it's echo but maybe some other thing someday
type Server struct {
	*echo.Echo
}

//Options has startup options for the server
type Options struct {
	Store store.BackerType
}

var defaultOptions = &Options{
	Store: store.SimpleStore,
}

//GetServer returns a new server instance
func GetServer(opts *Options) (srv *Server) {
	if opts == nil {
		opts = defaultOptions
	}
	store := store.NewStore(opts.Store)
	srv = &Server{
		echo.New(),
	}
	srv.Use(middleware.Logger())
	srv.Use(middleware.Recover())
	srv.Static("/", "./client/build")
	srv.GET("/pad/:id", control.NewHandler(store))
	srv.GET("/status", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"everything": "is okay!",
			"pads":       store.PadCount(),
			"subs":       store.SubsCount(),
		})
	})
	return
}
