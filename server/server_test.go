package server

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/phayes/freeport"
	"gitlab.com/note32/store"
)

//startServer gets the http server up on a free port and returns that port
//It will return an error if one occurs for handling in the calling test
func startServer() (port int, err error) {
	port, err = freeport.GetFreePort()
	if err != nil {
		return
	}
	srv := GetServer(&Options{
		Store: store.MemoryStore,
	})
	ch := make(chan error, 1)
	go func() {
		ch <- srv.Start(fmt.Sprintf(":%d", port))
	}()
	for true {
		select {
		case err = <-ch:
			return
		default:
			_, err = http.Get(
				fmt.Sprintf("http://127.0.0.1:%d", port),
			)
			if err != nil {
				//Before the server starts we expect a refused connection
				if !strings.HasSuffix(err.Error(), "connection refused") {
					return
				}
			} else {
				return
			}
		}
		time.Sleep(100 * time.Millisecond)
	}
	return
}

func TestStartup(t *testing.T) {
	port, err := startServer()
	if err != nil {
		t.Error(err)
	}
	res, err := http.Get(
		fmt.Sprintf("http://127.0.0.1:%d/status", port),
	)
	if res.StatusCode != 200 {
		bdy, _ := ioutil.ReadAll(res.Body)
		t.Error(errors.New(string(bdy)))
	}
}
