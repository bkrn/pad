import './main.css';
import { Main } from './Main.elm';

const isLocalhost = Boolean(
    window.location.hostname === 'localhost' ||
      // [::1] is the IPv6 localhost address.
      window.location.hostname === '[::1]' ||
      // 127.0.0.1/8 is considered localhost for IPv4.
      window.location.hostname.match(
        /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
      )
  );

function getwsuri() {
    const loc = window.location;
    let wsuri = "";
    if (loc.protocol === "https:") {
        wsuri = "wss:";
    } else if (!isLocalhost) {
        loc.protocol = "https:";
        wsuri = "wss:";
    } else {
        wsuri = "ws:";
    }
    wsuri += "//" + loc.host;
    return wsuri
}

const wsuri = getwsuri();
const storageName = 'note32-save';
const storedState = localStorage.getItem(storageName);
const state = storedState ? JSON.parse(storedState) : [];
const seed =  Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);

const app = Main.embed(document.getElementById('root'),{seed: seed, state: state, host: wsuri});




app.ports.setStorage.subscribe(function(state) {
    localStorage.setItem(storageName, JSON.stringify(state));
});
