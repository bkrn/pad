module Cipher exposing (..)

import Crypto.Strings exposing (decrypt, encrypt)
import Crypto.Strings.Types exposing (Passphrase, Plaintext, Ciphertext)
import Random exposing (Seed, initialSeed)


doEncrypt : Seed -> Passphrase -> Plaintext -> Result String ( Ciphertext, Seed )
doEncrypt seed passphrase plaintext =
    encrypt seed passphrase plaintext

doDecrypt : Passphrase -> Ciphertext -> Result String Plaintext
doDecrypt passphrase ciphertext =
    decrypt passphrase ciphertext