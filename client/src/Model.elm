module Model exposing (..)

import Random.Pcg as Rand
import Random as Random
import Uuid exposing (Uuid)


type alias PadID =
    { identity : String
    , name : String
    , secret : String
    }


type alias Writer =
    { identity : String
    , name : String
    }


type alias Line =
    { writer : Writer
    , number : Int
    , text : List Int
    }


type alias Pad =
    { identity : String
    , name : String
    , secret : String
    , lines : List Line
    , currentLine : Int
    }

padFromID : PadID -> Pad
padFromID pid =
    Pad pid.identity pid.name pid.secret [] 0

nullPad : Pad
nullPad =
    Pad "" "" "" [] 0


newPad : Uuid -> Pad
newPad uuid =
    { identity = Uuid.toString uuid
    , name = ""
    , secret = ""
    , lines = []
    , currentLine = 0
    }


type alias Model =
    { pads : List PadID
    , currentSeed : Rand.Seed
    , currentPad : Pad
    , lineInput : String
    , init : Bool
    , cipherSeed : Random.Seed
    , host : String
    , enteredID : String
    , idError : String
    }


nullModel : Int -> String -> Model
nullModel seed host =
    { currentSeed = Rand.initialSeed seed
    , pads = []
    , currentPad = nullPad
    , lineInput = ""
    , init = False
    , cipherSeed = Random.initialSeed seed
    , host = host
    , enteredID = ""
    , idError = ""
    }
