module Main exposing (..)

import Html exposing (Html)
import Html.Attributes as Attr
import Html.Events as Event
import Json.Decode as JD
import Json.Decode.Pipeline as JDP
import WebSocket
import Uuid
import Random.Pcg as Rand
import Model exposing (Model)
import Ports
import Bytes exposing (Bytes)
import Cipher
import Random
import Dom
import Dom.Scroll as Scroll
import Task


---- MODEL ----


init : { seed : Int, state : List Model.PadID, host : String } -> ( Model, Cmd Msg )
init flags =
    let
        mdl =
            (Model.nullModel flags.seed flags.host)
    in
        case List.head flags.state of
            Just pad ->
                { mdl
                    | currentPad = Model.Pad pad.identity pad.name pad.secret [] 0
                    , pads = flags.state
                }
                    ! []

            Nothing ->
                update NewPad mdl


writerDecoder : JD.Decoder Model.Writer
writerDecoder =
    JDP.decode Model.Writer
        |> JDP.required "Identity" JD.string
        |> JDP.required "Alias" JD.string


lineDecoder : JD.Decoder Model.Line
lineDecoder =
    JDP.decode Model.Line
        |> JDP.required "Writer" writerDecoder
        |> JDP.required "Number" JD.int
        |> JDP.required "Text" (JD.list JD.int)


decodeInput : JD.Decoder ( Int, String )
decodeInput =
    JD.map2 (,) (JD.at [ "keyCode" ] JD.int) (JD.at [ "target", "value" ] JD.string)


padDecoder : JD.Decoder Model.Pad
padDecoder =
    JDP.decode Model.Pad
        |> JDP.required "id" JD.string
        |> JDP.hardcoded ""
        |> JDP.hardcoded ""
        |> JDP.required "lines" (JD.list lineDecoder)
        |> JDP.required "currentLine" JD.int


blend : Model -> Model.Pad -> Model
blend mdl pad =
    let
        cpad =
            mdl.currentPad

        npad =
            { cpad | lines = pad.lines, currentLine = pad.currentLine }

        scrt =
            case List.head mdl.pads of
                Just fpad ->
                    fpad.secret

                Nothing ->
                    ""

        pid =
            (padToID npad)
    in
        { mdl
            | currentPad = npad
            , pads = { pid | secret = scrt } :: (List.drop 1 mdl.pads)
            , init = True
        }


populate : Model -> String -> Model
populate mdl data =
    let
        dec =
            JD.decodeString padDecoder data
    in
        case dec of
            Ok pad ->
                blend mdl pad

            Err _ ->
                mdl



----


type Msg
    = NoOp
    | NewPub String
    | NewPad
    | EnterID String
    | DoInput String String
    | ChangeName String
    | ChangePad Int
    | RemovePad Int
    | ChangeSecret String
    | AfterScroll (Result Dom.Error ())


{-| We setStorage` on every update. That's gonna be pretty wasteful
-}
updateWithStorage : Msg -> Model -> ( Model, Cmd Msg )
updateWithStorage msg model =
    let
        ( newModel, cmds ) =
            update msg model
    in
        ( newModel
        , Cmd.batch [ Ports.setStorage newModel.pads, cmds ]
        )


padToID : Model.Pad -> Model.PadID
padToID pad =
    Model.PadID pad.identity pad.name pad.secret


decrement : Model.Pad -> Int
decrement pad =
    if pad.currentLine == 0 then
        31
    else
        pad.currentLine - 1


increment : Int -> Int
increment num =
    if num == 31 then
        0
    else
        num + 1


insertLine : Model -> String -> Model
insertLine mdl data =
    let
        pad =
            mdl.currentPad
    in
        case JD.decodeString lineDecoder data of
            Ok ln ->
                { mdl
                    | currentPad =
                        { pad
                            | lines =
                                List.map
                                    (\l ->
                                        if l.number == ln.number then
                                            ln
                                        else
                                            l
                                    )
                                    pad.lines
                            , currentLine = increment ln.number
                        }
                }

            Err _ ->
                mdl


getScroll : Model -> Float
getScroll mdl =
    let
        ln =
            increment mdl.currentPad.currentLine
    in
        if ln < 12 then
            0
        else
            ((toFloat ln) - 12) * 36


getNewID : Model -> ( Model, Uuid.Uuid )
getNewID mdl =
    let
        prs =
            Uuid.fromString mdl.enteredID
    in
        case prs of
            Just uid ->
                ( mdl, uid )

            Nothing ->
                let
                    ( newUuid, newSeed ) =
                        Rand.step Uuid.uuidGenerator mdl.currentSeed
                in
                    ( { mdl | currentSeed = newSeed }, newUuid )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg mdl =
    case msg of
        EnterID val ->
            case Uuid.fromString val of
                Just _ ->
                    { mdl | enteredID = val } ! []

                Nothing ->
                    { mdl | enteredID = val, idError = (if val == "" then "" else "ID must be valid UUID") } ! []

        NewPad ->
            let
                ( nmdl, newUuid ) =
                    getNewID mdl

                newPad =
                    Model.newPad newUuid
            in
                ( { nmdl
                    | currentPad = newPad
                    , pads = (padToID newPad) :: mdl.pads
                    , init = False
                    , enteredID = ""
                    , idError = ""
                  }
                , Cmd.none
                )

        NewPub data ->
            if mdl.init then
                (insertLine mdl data) ! [ (Task.attempt AfterScroll <| Scroll.toY "pad-scroll" <| getScroll mdl) ]
            else
                let
                    nmdl =
                        populate mdl data

                    cpad =
                        nmdl.currentPad

                    smdl =
                        { mdl | currentPad = { cpad | currentLine = nmdl.currentPad.currentLine } }
                in
                    nmdl ! [ (Task.attempt AfterScroll <| Scroll.toY "pad-scroll" <| getScroll smdl) ]

        ChangePad ix ->
            let
                pads =
                    (List.drop ix mdl.pads) ++ (List.take ix mdl.pads)

                mpad =
                    List.head <| List.drop ix mdl.pads
            in
                case mpad of
                    Just pad ->
                        if pad.identity == mdl.currentPad.identity then
                            mdl ! []
                        else
                            { mdl | pads = pads, currentPad = (Model.padFromID pad), init = False } ! []

                    Nothing ->
                        mdl ! []

        RemovePad ix ->
            let
                pads =
                    (List.drop (ix + 1) mdl.pads) ++ (List.take (ix) mdl.pads)
            in
                { mdl | pads = pads } ! []

        ChangeName nm ->
            let
                pad =
                    mdl.currentPad

                padID =
                    padToID pad
            in
                ( { mdl
                    | currentPad = { pad | name = nm }
                    , pads = { padID | name = nm } :: (List.drop 1 mdl.pads)
                  }
                , Cmd.none
                )

        ChangeSecret scrt ->
            let
                pad =
                    mdl.currentPad

                padID =
                    padToID pad
            in
                ( { mdl
                    | currentPad = { pad | secret = scrt }
                    , pads = { padID | secret = scrt } :: (List.drop 1 mdl.pads)
                  }
                , Cmd.none
                )

        DoInput keyCode val ->
            let
                plain =
                    mdl.currentPad.secret == ""

                url =
                    mdl.host ++ "/pad/" ++ mdl.currentPad.identity

                lmt =
                    if plain then
                        80
                    else
                        32
            in
                if keyCode == "Enter" then
                    case cipherText mdl val of
                        Ok ( txt, seed ) ->
                            { mdl | lineInput = "", cipherSeed = seed }
                                ! [ WebSocket.send url txt
                                  ]

                        Err _ ->
                            mdl ! []
                else if keyCode == "Backspace" then
                    { mdl | lineInput = String.dropRight 1 val } ! []
                else if String.length val < lmt then
                    { mdl | lineInput = (val ++ keyCode) } ! []
                else
                    mdl ! []

        _ ->
            mdl ! []



---- VIEW ----


nameOrID : { a | name : String, identity : String } -> String
nameOrID pid =
    if pid.name /= "" then
        pid.name
    else
        pid.identity


drawPads : Model -> List (Html Msg)
drawPads mdl =
    List.indexedMap
        (\ix pid ->
            Html.div [ Attr.class "pad-desc" ]
                [ Html.a
                    [ Attr.class "pad-select", Event.onClick <| ChangePad ix ]
                    [ Html.text <| nameOrID pid ]
                , Html.a [ Attr.class "pad-remove", Event.onClick <| RemovePad ix ] [ Html.text "X" ]
                ]
        )
        mdl.pads


listToString : List Int -> String
listToString l =
    let
        lst =
            List.filter (\b -> b /= 0) l
    in
        case Bytes.fromList lst of
            Ok b ->
                Bytes.toString b

            _ ->
                ""


decodeLine : String -> String -> String
decodeLine passphrase ciphertext =
    case Cipher.doDecrypt passphrase ciphertext of
        Ok text ->
            text

        Err e ->
            toString e


lineText : Model -> Model.Line -> String
lineText mdl ln =
    let
        txt =
            listToString ln.text
    in
        if mdl.currentPad.secret == "" then
            String.trim txt
        else
            String.trim <| decodeLine mdl.currentPad.secret txt


encodeLineText : Model -> String -> Result String ( String, Random.Seed )
encodeLineText mdl ln =
    Cipher.doEncrypt mdl.cipherSeed mdl.currentPad.secret ln


cipherText : Model -> String -> Result String ( String, Random.Seed )
cipherText mdl ln =
    let
        lntxt =
            String.padRight 96 ' ' ln
    in
        if mdl.currentPad.secret == "" then
            Ok ( lntxt, mdl.cipherSeed )
        else
            encodeLineText mdl ln


drawLines : Model -> List (Html Msg)
drawLines mdl =
    List.indexedMap
        (\ix ln ->
            let
                sty =
                    (if ix == mdl.currentPad.currentLine then
                        [ ( "font-weight", "bold" ), ( "background-color", "Gainsboro" ) ]
                     else
                        []
                    )

                lt =
                    lineText mdl ln
            in
                Html.p []
                    [ Html.span [ Attr.style sty, Attr.class "line-number" ]
                        [ Html.text <| (++) (toString ix) "."
                        ]
                    , Html.span [ Attr.class "line-text" ] [ Html.text <| lineText mdl ln ]
                    ]
        )
        mdl.currentPad.lines


onLineInput : (String -> String -> msg) -> Html.Attribute msg
onLineInput tagger =
    Event.on "keypress" (JD.map2 tagger (JD.field "key" JD.string) Event.targetValue)


view : Model -> Html Msg
view mdl =
    Html.main_ []
        [ Html.section [ Attr.class "side-bar" ]
            [ Html.header [ Attr.class "nav-header" ] [ Html.text "bkrn.pad" ]
            , Html.nav [ Attr.class "nav" ] <|
                [ Html.button [ Attr.class "pad-button", Event.onClick NewPad, Attr.disabled (mdl.idError /= "") ]
                    [ Html.text (if mdl.enteredID /= "" then "load pad" else "new pad")
                    ]
                , Html.input
                    [ Attr.class "pad-id-entry"
                    , Event.onInput EnterID
                    , Attr.placeholder "Custom ID"
                    , Attr.value mdl.enteredID
                    ]
                    []
                , Html.p [ Attr.class "pad-id-error" ] [ Html.text mdl.idError ]
                ]
                    ++ (drawPads mdl)
            ]
        , Html.section [ Attr.class "pad" ]
            [ Html.input
                [ Attr.class "pad-header"
                , Attr.type_ "text"
                , Event.onInput ChangeName
                , Attr.value <| .name <| .currentPad mdl
                , Attr.placeholder "alias"
                ]
                []
            , Html.div []
                [ Html.caption [ Attr.class "pad-identity" ] [ Html.text <| "ID: " ++ mdl.currentPad.identity ]
                , Html.input
                    [ Attr.type_ "password"
                    , Attr.class "pad-secret"
                    , Attr.placeholder "secret"
                    , Event.onInput ChangeSecret
                    , Attr.value mdl.currentPad.secret
                    ]
                    [ Html.text mdl.currentPad.secret ]
                ]
            , Html.article [ Attr.class "pad-body" ]
                [ Html.div
                    [ Attr.id "pad-scroll"
                    , Attr.style
                        [ ( "overflow-y", "scroll" )
                        , ( "height", "30em" )
                        , ( "width", "90ch" )
                        ]
                    ]
                  <|
                    drawLines mdl
                ]
            , Html.input
                [ Attr.type_ "text"
                , Attr.class "pad-input"
                , Attr.placeholder "new line"
                , Attr.value mdl.lineInput
                , Attr.size
                    (if mdl.currentPad.secret == "" then
                        80
                     else
                        32
                    )
                , onLineInput DoInput
                ]
                []
            ]
        ]



---- PROGRAM ----


subscriptions : Model -> Sub Msg
subscriptions mdl =
    let
        url =
            mdl.host ++ "/pad/" ++ mdl.currentPad.identity
    in
        case Uuid.fromString mdl.currentPad.identity of
            Just _ ->
                WebSocket.listen (url) NewPub

            Nothing ->
                Sub.none


main : Program { seed : Int, state : List Model.PadID, host : String } Model Msg
main =
    Html.programWithFlags
        { init = init
        , view = view
        , update = updateWithStorage
        , subscriptions = subscriptions
        }
