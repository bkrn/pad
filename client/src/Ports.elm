port module Ports exposing (..)

import Model exposing (Model)

port setStorage : List Model.PadID -> Cmd msg