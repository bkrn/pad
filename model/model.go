package model

import (
	"crypto/rand"
	"sync"

	"github.com/satori/go.uuid"
)

//Writer is the user object
type Writer struct {
	Identity uuid.UUID
	Alias    string
}

//Pad is a notepad
type Pad struct {
	*sync.RWMutex `json:"-"`
	ID            uuid.UUID `json:"id"`
	CurrentLine   uint8     `json:"currentLine"`
	Lines         [32]Line  `json:"lines"`
}

//Insert inserts a new line
func (pad *Pad) Insert(ln Line) Line {
	pad.Lock()
	ln.Number = pad.CurrentLine
	pad.Lines[pad.CurrentLine] = ln
	if pad.CurrentLine == 31 {
		pad.CurrentLine = 0
	} else {
		pad.CurrentLine++
	}
	pad.Unlock()
	return ln
}

//NewPad creates a new pad with a given uuid
func NewPad(id uuid.UUID) *Pad {
	return &Pad{
		ID:      id,
		Lines:   NewLines(),
		RWMutex: &sync.RWMutex{},
	}
}

//Line is an item in a notepad
type Line struct {
	Writer Writer
	Number uint8
	Text   [80]byte
}

//randLine generates random line bytes
func randLine() [80]byte {
	ln := [80]byte{}
	slc := make([]byte, len(ln))
	//Ignored error
	rand.Read(slc)
	for ix, b := range slc {
		ln[ix] = b
	}
	return ln
}

//NewLines generates 32 random lines
func NewLines() (lns [32]Line) {
	//Ignored error
	wrtr := Writer{
		Identity: uuid.Nil,
		Alias:    "bkrn",
	}
	for ix := range lns {
		lns[ix] = Line{
			Writer: wrtr,
			Number: uint8(ix),
			Text:   randLine(),
		}
	}
	return lns
}
