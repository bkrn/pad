package control

import (
	"net/http"

	"github.com/satori/go.uuid"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo"
	"gitlab.com/note32/model"
	"gitlab.com/note32/store"
)

var (
	upgrader = websocket.Upgrader{}
)

//NewHandler creates a new control creation function
func NewHandler(store store.Store) func(echo.Context) error {
	return func(c echo.Context) error {
		return startControl(c, store)
	}
}

//startControl begins a new control session
func startControl(c echo.Context, store store.Store) error {
	id, err := uuid.FromString(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	toWrite := make(chan model.Line)
	endWrite := make(chan struct{})
	pad, sub := store.Get(id)
	toPublish := sub.Inbox
	ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	defer ws.Close()
	defer close(toWrite)
	defer close(endWrite)
	defer store.Kill(pad.ID, sub.ID)

	err = ws.WriteJSON(pad)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	go func() {
		for {
			_, msg, err := ws.ReadMessage()
			txt := [80]byte{}
			for ix, b := range msg {
				if ix == 80 {
					break
				}
				txt[ix] = b
			}
			ln := model.Line{
				Text: txt,
			}
			if err == nil {
				toWrite <- ln
			} else {
				break
			}
		}
		endWrite <- struct{}{}
	}()

	for {
		select {
		case ln := <-toWrite:
			store.Put(id, ln)
		case ln := <-toPublish:
			ws.WriteJSON(ln)
		case <-endWrite:
			return nil
		}
	}
}
