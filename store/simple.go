package store

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"sync"

	"github.com/aws/aws-sdk-go/aws/credentials"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/note32/model"
	"gitlab.com/note32/settings"
)

type simpleStore struct {
	s3client *s3.S3
	memStore
}

func ensureBucket(client *s3.S3) error {
	bkcs, err := client.ListBuckets(&s3.ListBucketsInput{})
	if err != nil {
		return err
	}
	var fnd bool
	for _, bck := range bkcs.Buckets {
		if fnd = *bck.Name == settings.AppCfg.Store.Bucket; fnd {
			break
		}
	}
	if !fnd {
		client.CreateBucket(
			&s3.CreateBucketInput{
				Bucket: aws.String(settings.AppCfg.Store.Bucket),
			},
		)
	}
	return err
}

func newSimpleStore() *simpleStore {
	awscfg := &aws.Config{
		Region: aws.String(settings.AppCfg.Store.Region),
	}
	if settings.AppCfg.Env != "prod" {
		awscfg.Endpoint = aws.String(
			"http://127.0.0.1:4572",
		)
		awscfg.S3ForcePathStyle = aws.Bool(true)
		awscfg.Credentials = credentials.NewEnvCredentials()
	}
	svc := s3.New(session.New(), awscfg)
	ensureBucket(svc)
	return &simpleStore{svc, *newMemStore()}
}

func (str *simpleStore) Put(id uuid.UUID, ln model.Line) {
	str.memStore.Put(id, ln)
	pd := str.getPad(id)
	//ignored error
	bdy, err := json.Marshal(pd)
	if err != nil {
		log.Println("Pad put serialization error: " + err.Error())
	}
	str.storeLock.Lock()
	inpt := &s3.PutObjectInput{
		Bucket: aws.String(settings.AppCfg.Store.Bucket),
		Body:   bytes.NewReader(bdy),
		Key:    aws.String(id.String()),
	}
	_, err = str.s3client.PutObject(inpt)
	if err != nil {
		log.Println("Pad put write error: " + err.Error())
	}
	str.storeLock.Unlock()
}

func (str *simpleStore) Get(id uuid.UUID) (model.Pad, *Subscription) {
	if !str.exists(id) {
		inpt := &s3.GetObjectInput{
			Bucket: aws.String(settings.AppCfg.Store.Bucket),
			Key:    aws.String(id.String()),
		}
		out, err := str.s3client.GetObject(inpt)
		if err == nil {
			defer out.Body.Close()
			b, _ := ioutil.ReadAll(out.Body)
			pad := &model.Pad{}
			json.Unmarshal(b, pad)
			pad.RWMutex = &sync.RWMutex{}
			str.storeLock.Lock()
			str.store[id] = pad
			str.storeLock.Unlock()
		}
	}
	pd, sub := str.memStore.Get(id)
	return pd, sub
}
