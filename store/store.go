package store

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/note32/model"
)

//BackerType describes valid store types
type BackerType int

//StoreType enum
const (
	MemoryStore BackerType = iota
	SimpleStore
)

//Subscription is a subscription to new lines written to a pad
type Subscription struct {
	ID    uuid.UUID
	Inbox chan model.Line
}

//Check returns either a line or nil and fnd non blocking
func (sub *Subscription) Check() (*model.Line, bool) {
	select {
	case ln := <-sub.Inbox:
		return &ln, true
	default:
		return nil, true
	}
}

//Get returns the next line and is blocking
func (sub *Subscription) Get() (*model.Line, bool) {
	for ln := range sub.Inbox {
		return &ln, true
	}
	return nil, false
}

//Store is the common interface to the various store types
type Store interface {
	Get(id uuid.UUID) (model.Pad, *Subscription)
	Put(id uuid.UUID, ln model.Line)
	PadCount() int
	SubsCount() int
	Kill(padid uuid.UUID, subid uuid.UUID)
}

//NewStore returns a new store of the specified type
func NewStore(bt BackerType) Store {
	switch bt {
	case MemoryStore:
		return newMemStore()
	case SimpleStore:
		return newSimpleStore()
	default:
		return newMemStore()
	}
}
