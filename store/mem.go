package store

import (
	"sync"

	"github.com/satori/go.uuid"
	"gitlab.com/note32/model"
)

type memStore struct {
	store     map[uuid.UUID]*model.Pad
	subs      map[uuid.UUID][]*Subscription
	subLock   *sync.RWMutex
	storeLock *sync.RWMutex
}

//newMemStore returns a new store that operates in local memory
func newMemStore() *memStore {
	return &memStore{
		store:     map[uuid.UUID]*model.Pad{},
		subs:      map[uuid.UUID][]*Subscription{},
		subLock:   &sync.RWMutex{},
		storeLock: &sync.RWMutex{},
	}
}

func (str *memStore) exists(id uuid.UUID) (fnd bool) {
	str.storeLock.RLock()
	_, fnd = str.store[id]
	str.storeLock.RUnlock()
	return
}

func (str *memStore) getPad(id uuid.UUID) *model.Pad {
	str.storeLock.RLock()
	var pad *model.Pad
	var fnd bool
	if pad, fnd = str.store[id]; !fnd {
		pad = model.NewPad(id)
		str.store[id] = pad
	}
	str.storeLock.RUnlock()
	return pad
}

func (str *memStore) getSub(id uuid.UUID) *Subscription {
	str.subLock.Lock()
	chn := make(chan model.Line, 10)
	subid, _ := uuid.NewV4()
	sub := &Subscription{
		subid, chn,
	}
	str.subs[id] = append(str.subs[id], sub)
	str.subLock.Unlock()
	return sub
}

func (str *memStore) Get(id uuid.UUID) (model.Pad, *Subscription) {
	pad := str.getPad(id)
	sub := str.getSub(id)
	return *pad, sub
}

func (str *memStore) Put(id uuid.UUID, ln model.Line) {
	pad := str.getPad(id)
	ln = pad.Insert(ln)
	str.subLock.RLock()
	for _, sub := range str.subs[id] {
		sub.Inbox <- ln
	}
	str.subLock.RUnlock()
}

func (str *memStore) Kill(padid uuid.UUID, subid uuid.UUID) {
	var subs = []*Subscription{}
	str.subLock.Lock()
	for _, sub := range str.subs[padid] {
		if sub.ID == subid {
			close(sub.Inbox)
			continue
		}
		subs = append(subs, sub)
	}
	str.subs[padid] = subs
	str.subLock.Unlock()
}

func (str *memStore) PadCount() (cnt int) {
	str.storeLock.RLock()
	cnt = len(str.store)
	str.storeLock.RUnlock()
	return
}

func (str *memStore) SubsCount() (cnt int) {
	str.subLock.RLock()
	for _, v := range str.subs {
		cnt += len(v)
	}
	str.subLock.RUnlock()
	return
}
